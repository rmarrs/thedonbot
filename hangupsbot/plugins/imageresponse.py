import logging, os, random, urllib.request
from bs4 import BeautifulSoup
import random
import hangups
import plugins

logger = logging.getLogger(__name__)

_externals = { "running": False }


def _initialise(bot):
    plugins.register_user_command(["jahadu"])
    plugins.register_user_command(["ai8w"])


def jahadu(bot, event, *args):
    """Searches for a meme related to <something>;
    grabs a random meme when none provided"""
    if _externals["running"]:
        yield from bot.coro_send_message(event.conv_id, "<i>busy, give me a moment...</i>")
        return

    _externals["running"] = True

    try:

        rnd = random.randrange(1,15)
       

        instance_link = "http://ec2-52-36-94-180.us-west-2.compute.amazonaws.com/images/jahadu/" + str(rnd) + ".png"

        jpg_link = instance_link

        image_data = urllib.request.urlopen(jpg_link)
        filename = os.path.basename(jpg_link)

        #legacy_segments = [hangups.ChatMessageSegment(instance_link, hangups.SegmentType.LINK, link_target=instance_link)]

        logger.debug("uploading {} from {}".format(filename, jpg_link))
        photo_id = yield from bot._client.upload_image(image_data, filename=filename)

        yield from bot.coro_send_message(event.conv.id_, "", image_id=photo_id)
    except Exception as e:
        yield from bot.coro_send_message(event.conv_id, "<i>Jahadu is overheating.... cooling down...</i>")
        logger.exception("FAILED TO RETRIEVE MEME")

    finally:
        _externals["running"] = False


def ai8w(bot, event, *args):
    """Searches for a meme related to <something>;
    grabs a random meme when none provided"""
    if _externals["running"]:
        yield from bot.coro_send_message(event.conv_id, "<i>busy, give me a moment...</i>")
        return

    _externals["running"] = True

    try:
       
        instance_link = "http://ec2-52-36-94-180.us-west-2.compute.amazonaws.com/images/ai8w.gif"

        jpg_link = instance_link

        image_data = urllib.request.urlopen(jpg_link)
        filename = os.path.basename(jpg_link)

        #legacy_segments = [hangups.ChatMessageSegment(instance_link, hangups.SegmentType.LINK, link_target=instance_link)]

        logger.debug("uploading {} from {}".format(filename, jpg_link))
        photo_id = yield from bot._client.upload_image(image_data, filename=filename)

        yield from bot.coro_send_message(event.conv.id_, "", image_id=photo_id)
    except Exception as e:
        yield from bot.coro_send_message(event.conv_id, "<i>Ai8w is busy filing a police report. Please hold.</i>")
        logger.exception("FAILED TO RETRIEVE MEME")

    finally:
        _externals["running"] = False
